import { assert, expect } from 'chai';

import MyLibrary from '../src/main';

var Instance;

describe( 'Tests the Node Library', () => {

  it( 'Demonstrates this test is working by instantiating the class', function() {

	  Instance = new MyLibrary();
	  assert( Instance instanceof MyLibrary, 'The library could not be instantiated' );

  });

});
